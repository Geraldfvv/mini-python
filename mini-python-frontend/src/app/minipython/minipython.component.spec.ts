import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MinipythonComponent } from './minipython.component';

describe('MinipythonComponent', () => {
  let component: MinipythonComponent;
  let fixture: ComponentFixture<MinipythonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MinipythonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MinipythonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
