import os
from gen.Scanner import *
from MyVisitor import *
from antlr4 import *
from flask import Flask, request
from flask_cors import CORS
from antlr4.error.ErrorListener import ErrorListener

# Initialize server
app = Flask(__name__, static_url_path='')
UPLOAD_FOLDER = './UPLOAD_FOLDER'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
path = os.getcwd() + "/UPLOAD_FOLDER"
if not os.path.exists(path):
    os.mkdir(path)
CORS(app)


class MyErrorListener(ErrorListener):

    def __init__(self):

        self.newErrors = []
        super(MyErrorListener, self).__init__()

    def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e):
        if isinstance(recognizer, miniPython):
            self.newErrors.append("PARSER ERROR - line " + str(line) + ":" + str(column) + " " + msg)
        else:
            if isinstance(recognizer, Scanner):
                self.newErrors.append("SCANNER ERROR - line " + str(line) + ":" + str(column) + " " + msg)
            else:
                self.newErrors.append("OTHER ERROR - line " + str(line) + ":" + str(column) + " " + msg)


@app.route('/checkFile', methods=["POST"])
def checkFile():
    try:
        myListener = MyErrorListener()
        file = request.files['file']
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))
        path = "./UPLOAD_FOLDER/" + file.filename

        input = FileStream(path)

        inst = Scanner(input)
        tokens = CommonTokenStream(inst)
        parser = miniPython(tokens)

        inst.removeErrorListeners()
        inst.addErrorListener(myListener)

        parser.removeErrorListeners()
        parser.addErrorListener(myListener)

        print("ARBOL")
        tree = parser.program()
        # print(Trees.toStringTree(tree, None, parser))

        print("VISITOR")
        visitor = MyVisitor()
        visitor.visit(tree)

        for i in visitor.errors:
            print(i)

        # tokensList = printTokens(inst.getAllTokens())

        if os.path.exists(path):
            os.remove(path)
        errors = myListener.newErrors
        return {'tokens': "tokens",
                'errors': errors}, 200
    except Exception as e:
        return {'message': str(e)}, 400


def printTokens(tokens):
    tokensResponse = []
    print("---TOKENS---")
    for token in tokens:
        tokenText = str(token.type) + "-" + token.text
        tokensResponse.append(tokenText)
        print(tokenText)
    return tokensResponse


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
