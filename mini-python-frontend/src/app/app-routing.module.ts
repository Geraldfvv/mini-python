import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MinipythonComponent } from './minipython/minipython.component';

const routes: Routes = [
  {path:'' , redirectTo: 'minipython', pathMatch:'full'},
  {path:'minipython' , component: MinipythonComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
