# Generated from C:/Users/geral/Desktop/Proyecto/mini-python/mini-python-backend\miniPython.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3%")
        buf.write("\u00d1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\3\2\3\2\3\2\3")
        buf.write("\3\6\3\63\n\3\r\3\16\3\64\3\4\3\4\3\4\3\4\3\4\3\4\3\4")
        buf.write("\3\4\3\4\5\4@\n\4\3\5\3\5\3\5\3\5\5\5F\n\5\3\5\3\5\3\5")
        buf.write("\3\5\3\6\3\6\3\6\7\6O\n\6\f\6\16\6R\13\6\3\7\3\7\3\7\3")
        buf.write("\7\3\7\3\7\3\7\5\7[\n\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3")
        buf.write("\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13")
        buf.write("\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16")
        buf.write("\3\16\3\17\3\17\3\17\3\17\3\20\6\20\u0084\n\20\r\20\16")
        buf.write("\20\u0085\3\21\3\21\3\21\7\21\u008b\n\21\f\21\16\21\u008e")
        buf.write("\13\21\3\22\3\22\3\22\7\22\u0093\n\22\f\22\16\22\u0096")
        buf.write("\13\22\3\23\3\23\3\23\7\23\u009b\n\23\f\23\16\23\u009e")
        buf.write("\13\23\3\24\3\24\3\24\3\24\3\24\7\24\u00a5\n\24\f\24\16")
        buf.write("\24\u00a8\13\24\3\25\3\25\3\25\7\25\u00ad\n\25\f\25\16")
        buf.write("\25\u00b0\13\25\3\25\5\25\u00b3\n\25\3\26\5\26\u00b6\n")
        buf.write("\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26")
        buf.write("\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u00c8\n\26\3\27\3")
        buf.write("\27\3\27\3\27\3\27\5\27\u00cf\n\27\3\27\2\2\30\2\4\6\b")
        buf.write("\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,\2\5\4\2\r\r\30")
        buf.write("\33\4\2\24\24\26\26\4\2\25\25\27\27\2\u00d4\2.\3\2\2\2")
        buf.write("\4\62\3\2\2\2\6?\3\2\2\2\bA\3\2\2\2\nK\3\2\2\2\fS\3\2")
        buf.write("\2\2\16\\\3\2\2\2\20a\3\2\2\2\22h\3\2\2\2\24l\3\2\2\2")
        buf.write("\26p\3\2\2\2\30u\3\2\2\2\32{\3\2\2\2\34~\3\2\2\2\36\u0083")
        buf.write("\3\2\2\2 \u0087\3\2\2\2\"\u008f\3\2\2\2$\u0097\3\2\2\2")
        buf.write("&\u009f\3\2\2\2(\u00b2\3\2\2\2*\u00c7\3\2\2\2,\u00c9\3")
        buf.write("\2\2\2./\5\4\3\2/\60\7\2\2\3\60\3\3\2\2\2\61\63\5\6\4")
        buf.write("\2\62\61\3\2\2\2\63\64\3\2\2\2\64\62\3\2\2\2\64\65\3\2")
        buf.write("\2\2\65\5\3\2\2\2\66@\5\b\5\2\67@\5\f\7\28@\5\22\n\29")
        buf.write("@\5\24\13\2:@\5\16\b\2;@\5\20\t\2<@\5\26\f\2=@\5\30\r")
        buf.write("\2>@\5\32\16\2?\66\3\2\2\2?\67\3\2\2\2?8\3\2\2\2?9\3\2")
        buf.write("\2\2?:\3\2\2\2?;\3\2\2\2?<\3\2\2\2?=\3\2\2\2?>\3\2\2\2")
        buf.write("@\7\3\2\2\2AB\7\3\2\2BC\7\36\2\2CE\7\16\2\2DF\5\n\6\2")
        buf.write("ED\3\2\2\2EF\3\2\2\2FG\3\2\2\2GH\7\17\2\2HI\7\23\2\2I")
        buf.write("J\5\34\17\2J\t\3\2\2\2KP\7\36\2\2LM\7\22\2\2MO\7\36\2")
        buf.write("\2NL\3\2\2\2OR\3\2\2\2PN\3\2\2\2PQ\3\2\2\2Q\13\3\2\2\2")
        buf.write("RP\3\2\2\2ST\7\4\2\2TU\5 \21\2UV\7\23\2\2VZ\5\34\17\2")
        buf.write("WX\7\5\2\2XY\7\23\2\2Y[\5\34\17\2ZW\3\2\2\2Z[\3\2\2\2")
        buf.write("[\r\3\2\2\2\\]\7\6\2\2]^\5 \21\2^_\7\23\2\2_`\5\34\17")
        buf.write("\2`\17\3\2\2\2ab\7\7\2\2bc\5 \21\2cd\7\b\2\2de\5(\25\2")
        buf.write("ef\7\23\2\2fg\5\34\17\2g\21\3\2\2\2hi\7\t\2\2ij\5 \21")
        buf.write("\2jk\7 \2\2k\23\3\2\2\2lm\7\n\2\2mn\5 \21\2no\7 \2\2o")
        buf.write("\25\3\2\2\2pq\7\36\2\2qr\7\f\2\2rs\5 \21\2st\7 \2\2t\27")
        buf.write("\3\2\2\2uv\7\36\2\2vw\7\16\2\2wx\5(\25\2xy\7\17\2\2yz")
        buf.write("\7 \2\2z\31\3\2\2\2{|\5(\25\2|}\7 \2\2}\33\3\2\2\2~\177")
        buf.write("\7$\2\2\177\u0080\5\36\20\2\u0080\u0081\7%\2\2\u0081\35")
        buf.write("\3\2\2\2\u0082\u0084\5\6\4\2\u0083\u0082\3\2\2\2\u0084")
        buf.write("\u0085\3\2\2\2\u0085\u0083\3\2\2\2\u0085\u0086\3\2\2\2")
        buf.write("\u0086\37\3\2\2\2\u0087\u008c\5\"\22\2\u0088\u0089\t\2")
        buf.write("\2\2\u0089\u008b\5\"\22\2\u008a\u0088\3\2\2\2\u008b\u008e")
        buf.write("\3\2\2\2\u008c\u008a\3\2\2\2\u008c\u008d\3\2\2\2\u008d")
        buf.write("!\3\2\2\2\u008e\u008c\3\2\2\2\u008f\u0094\5$\23\2\u0090")
        buf.write("\u0091\t\3\2\2\u0091\u0093\5$\23\2\u0092\u0090\3\2\2\2")
        buf.write("\u0093\u0096\3\2\2\2\u0094\u0092\3\2\2\2\u0094\u0095\3")
        buf.write("\2\2\2\u0095#\3\2\2\2\u0096\u0094\3\2\2\2\u0097\u009c")
        buf.write("\5&\24\2\u0098\u0099\t\4\2\2\u0099\u009b\5&\24\2\u009a")
        buf.write("\u0098\3\2\2\2\u009b\u009e\3\2\2\2\u009c\u009a\3\2\2\2")
        buf.write("\u009c\u009d\3\2\2\2\u009d%\3\2\2\2\u009e\u009c\3\2\2")
        buf.write("\2\u009f\u00a6\5*\26\2\u00a0\u00a1\7\20\2\2\u00a1\u00a2")
        buf.write("\5 \21\2\u00a2\u00a3\7\21\2\2\u00a3\u00a5\3\2\2\2\u00a4")
        buf.write("\u00a0\3\2\2\2\u00a5\u00a8\3\2\2\2\u00a6\u00a4\3\2\2\2")
        buf.write("\u00a6\u00a7\3\2\2\2\u00a7\'\3\2\2\2\u00a8\u00a6\3\2\2")
        buf.write("\2\u00a9\u00ae\5 \21\2\u00aa\u00ab\7\22\2\2\u00ab\u00ad")
        buf.write("\5 \21\2\u00ac\u00aa\3\2\2\2\u00ad\u00b0\3\2\2\2\u00ae")
        buf.write("\u00ac\3\2\2\2\u00ae\u00af\3\2\2\2\u00af\u00b3\3\2\2\2")
        buf.write("\u00b0\u00ae\3\2\2\2\u00b1\u00b3\3\2\2\2\u00b2\u00a9\3")
        buf.write("\2\2\2\u00b2\u00b1\3\2\2\2\u00b3)\3\2\2\2\u00b4\u00b6")
        buf.write("\7\26\2\2\u00b5\u00b4\3\2\2\2\u00b5\u00b6\3\2\2\2\u00b6")
        buf.write("\u00b7\3\2\2\2\u00b7\u00c8\7\35\2\2\u00b8\u00c8\7\37\2")
        buf.write("\2\u00b9\u00c8\5,\27\2\u00ba\u00bb\7\16\2\2\u00bb\u00bc")
        buf.write("\5 \21\2\u00bc\u00bd\7\17\2\2\u00bd\u00c8\3\2\2\2\u00be")
        buf.write("\u00bf\7\20\2\2\u00bf\u00c0\5(\25\2\u00c0\u00c1\7\21\2")
        buf.write("\2\u00c1\u00c8\3\2\2\2\u00c2\u00c3\7\13\2\2\u00c3\u00c4")
        buf.write("\7\16\2\2\u00c4\u00c5\5 \21\2\u00c5\u00c6\7\17\2\2\u00c6")
        buf.write("\u00c8\3\2\2\2\u00c7\u00b5\3\2\2\2\u00c7\u00b8\3\2\2\2")
        buf.write("\u00c7\u00b9\3\2\2\2\u00c7\u00ba\3\2\2\2\u00c7\u00be\3")
        buf.write("\2\2\2\u00c7\u00c2\3\2\2\2\u00c8+\3\2\2\2\u00c9\u00ce")
        buf.write("\7\36\2\2\u00ca\u00cb\7\16\2\2\u00cb\u00cc\5(\25\2\u00cc")
        buf.write("\u00cd\7\17\2\2\u00cd\u00cf\3\2\2\2\u00ce\u00ca\3\2\2")
        buf.write("\2\u00ce\u00cf\3\2\2\2\u00cf-\3\2\2\2\21\64?EPZ\u0085")
        buf.write("\u008c\u0094\u009c\u00a6\u00ae\u00b2\u00b5\u00c7\u00ce")
        return buf.getvalue()


class miniPythonParser ( Parser ):

    grammarFileName = "miniPython.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'def'", "'if'", "'else'", "'while'", 
                     "'for'", "'in'", "'return'", "'print'", "'len'", "'='", 
                     "'=='", "'('", "')'", "'['", "']'", "','", "':'", "'+'", 
                     "'*'", "'-'", "'/'", "'>'", "'<'", "'<='", "'>='" ]

    symbolicNames = [ "<INVALID>", "DEF", "IF", "ELSE", "WHILE", "FOR", 
                      "IN", "RETURN", "PRINT", "LEN", "ASSIGN", "EQUAL", 
                      "BLEFT", "BRIGHT", "SQLEFT", "SQRIGHT", "COMMA", "COLON", 
                      "SUM", "MUL", "SUB", "DIV", "HIGHER", "LESS", "LESSOREQUAL", 
                      "HIGHEROREQUAL", "CHAR", "NUM", "IDENTIFIER", "STRING", 
                      "NEWLINE", "WS", "BLOK_COMMENT", "COMMENT", "INDENT", 
                      "DEDENT" ]

    RULE_program = 0
    RULE_statements = 1
    RULE_statement = 2
    RULE_defStatement = 3
    RULE_argList = 4
    RULE_ifStatement = 5
    RULE_whileStatement = 6
    RULE_forStatement = 7
    RULE_returnStatement = 8
    RULE_printStatement = 9
    RULE_assignStatement = 10
    RULE_functionCallStatement = 11
    RULE_expressionStatement = 12
    RULE_sequence = 13
    RULE_moreStatements = 14
    RULE_expression = 15
    RULE_additionExpression = 16
    RULE_multiplicationExpression = 17
    RULE_elementExpression = 18
    RULE_expressionList = 19
    RULE_primitiveExpression = 20
    RULE_designator = 21

    ruleNames =  [ "program", "statements", "statement", "defStatement", 
                   "argList", "ifStatement", "whileStatement", "forStatement", 
                   "returnStatement", "printStatement", "assignStatement", 
                   "functionCallStatement", "expressionStatement", "sequence", 
                   "moreStatements", "expression", "additionExpression", 
                   "multiplicationExpression", "elementExpression", "expressionList", 
                   "primitiveExpression", "designator" ]

    EOF = Token.EOF
    DEF=1
    IF=2
    ELSE=3
    WHILE=4
    FOR=5
    IN=6
    RETURN=7
    PRINT=8
    LEN=9
    ASSIGN=10
    EQUAL=11
    BLEFT=12
    BRIGHT=13
    SQLEFT=14
    SQRIGHT=15
    COMMA=16
    COLON=17
    SUM=18
    MUL=19
    SUB=20
    DIV=21
    HIGHER=22
    LESS=23
    LESSOREQUAL=24
    HIGHEROREQUAL=25
    CHAR=26
    NUM=27
    IDENTIFIER=28
    STRING=29
    NEWLINE=30
    WS=31
    BLOK_COMMENT=32
    COMMENT=33
    INDENT=34
    DEDENT=35

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_program

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstProgramContext(ProgramContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.ProgramContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def statements(self):
            return self.getTypedRuleContext(miniPythonParser.StatementsContext,0)

        def EOF(self):
            return self.getToken(miniPythonParser.EOF, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstProgram" ):
                return visitor.visitAstProgram(self)
            else:
                return visitor.visitChildren(self)



    def program(self):

        localctx = miniPythonParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        try:
            localctx = miniPythonParser.AstProgramContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 44
            self.statements()
            self.state = 45
            self.match(miniPythonParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementsContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_statements

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstStatementDefContext(StatementsContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.StatementsContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(miniPythonParser.StatementContext)
            else:
                return self.getTypedRuleContext(miniPythonParser.StatementContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstStatementDef" ):
                return visitor.visitAstStatementDef(self)
            else:
                return visitor.visitChildren(self)



    def statements(self):

        localctx = miniPythonParser.StatementsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_statements)
        self._la = 0 # Token type
        try:
            localctx = miniPythonParser.AstStatementDefContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 48 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 47
                self.statement()
                self.state = 50 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << miniPythonParser.DEF) | (1 << miniPythonParser.IF) | (1 << miniPythonParser.WHILE) | (1 << miniPythonParser.FOR) | (1 << miniPythonParser.RETURN) | (1 << miniPythonParser.PRINT) | (1 << miniPythonParser.LEN) | (1 << miniPythonParser.BLEFT) | (1 << miniPythonParser.SQLEFT) | (1 << miniPythonParser.SUB) | (1 << miniPythonParser.NUM) | (1 << miniPythonParser.IDENTIFIER) | (1 << miniPythonParser.STRING) | (1 << miniPythonParser.NEWLINE))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def defStatement(self):
            return self.getTypedRuleContext(miniPythonParser.DefStatementContext,0)


        def ifStatement(self):
            return self.getTypedRuleContext(miniPythonParser.IfStatementContext,0)


        def returnStatement(self):
            return self.getTypedRuleContext(miniPythonParser.ReturnStatementContext,0)


        def printStatement(self):
            return self.getTypedRuleContext(miniPythonParser.PrintStatementContext,0)


        def whileStatement(self):
            return self.getTypedRuleContext(miniPythonParser.WhileStatementContext,0)


        def forStatement(self):
            return self.getTypedRuleContext(miniPythonParser.ForStatementContext,0)


        def assignStatement(self):
            return self.getTypedRuleContext(miniPythonParser.AssignStatementContext,0)


        def functionCallStatement(self):
            return self.getTypedRuleContext(miniPythonParser.FunctionCallStatementContext,0)


        def expressionStatement(self):
            return self.getTypedRuleContext(miniPythonParser.ExpressionStatementContext,0)


        def getRuleIndex(self):
            return miniPythonParser.RULE_statement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStatement" ):
                return visitor.visitStatement(self)
            else:
                return visitor.visitChildren(self)




    def statement(self):

        localctx = miniPythonParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_statement)
        try:
            self.state = 61
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 52
                self.defStatement()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 53
                self.ifStatement()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 54
                self.returnStatement()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 55
                self.printStatement()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 56
                self.whileStatement()
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 57
                self.forStatement()
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 58
                self.assignStatement()
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 59
                self.functionCallStatement()
                pass

            elif la_ == 9:
                self.enterOuterAlt(localctx, 9)
                self.state = 60
                self.expressionStatement()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DefStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_defStatement

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstDefStatmentContext(DefStatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.DefStatementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def DEF(self):
            return self.getToken(miniPythonParser.DEF, 0)
        def IDENTIFIER(self):
            return self.getToken(miniPythonParser.IDENTIFIER, 0)
        def BLEFT(self):
            return self.getToken(miniPythonParser.BLEFT, 0)
        def BRIGHT(self):
            return self.getToken(miniPythonParser.BRIGHT, 0)
        def COLON(self):
            return self.getToken(miniPythonParser.COLON, 0)
        def sequence(self):
            return self.getTypedRuleContext(miniPythonParser.SequenceContext,0)

        def argList(self):
            return self.getTypedRuleContext(miniPythonParser.ArgListContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstDefStatment" ):
                return visitor.visitAstDefStatment(self)
            else:
                return visitor.visitChildren(self)



    def defStatement(self):

        localctx = miniPythonParser.DefStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_defStatement)
        self._la = 0 # Token type
        try:
            localctx = miniPythonParser.AstDefStatmentContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 63
            self.match(miniPythonParser.DEF)
            self.state = 64
            self.match(miniPythonParser.IDENTIFIER)
            self.state = 65
            self.match(miniPythonParser.BLEFT)
            self.state = 67
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==miniPythonParser.IDENTIFIER:
                self.state = 66
                self.argList()


            self.state = 69
            self.match(miniPythonParser.BRIGHT)
            self.state = 70
            self.match(miniPythonParser.COLON)
            self.state = 71
            self.sequence()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ArgListContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_argList

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstArgListContext(ArgListContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.ArgListContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def IDENTIFIER(self, i:int=None):
            if i is None:
                return self.getTokens(miniPythonParser.IDENTIFIER)
            else:
                return self.getToken(miniPythonParser.IDENTIFIER, i)
        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(miniPythonParser.COMMA)
            else:
                return self.getToken(miniPythonParser.COMMA, i)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstArgList" ):
                return visitor.visitAstArgList(self)
            else:
                return visitor.visitChildren(self)



    def argList(self):

        localctx = miniPythonParser.ArgListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_argList)
        self._la = 0 # Token type
        try:
            localctx = miniPythonParser.AstArgListContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 73
            self.match(miniPythonParser.IDENTIFIER)
            self.state = 78
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==miniPythonParser.COMMA:
                self.state = 74
                self.match(miniPythonParser.COMMA)
                self.state = 75
                self.match(miniPythonParser.IDENTIFIER)
                self.state = 80
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IfStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_ifStatement

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstIfStatmentContext(IfStatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.IfStatementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def IF(self):
            return self.getToken(miniPythonParser.IF, 0)
        def expression(self):
            return self.getTypedRuleContext(miniPythonParser.ExpressionContext,0)

        def COLON(self, i:int=None):
            if i is None:
                return self.getTokens(miniPythonParser.COLON)
            else:
                return self.getToken(miniPythonParser.COLON, i)
        def sequence(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(miniPythonParser.SequenceContext)
            else:
                return self.getTypedRuleContext(miniPythonParser.SequenceContext,i)

        def ELSE(self):
            return self.getToken(miniPythonParser.ELSE, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstIfStatment" ):
                return visitor.visitAstIfStatment(self)
            else:
                return visitor.visitChildren(self)



    def ifStatement(self):

        localctx = miniPythonParser.IfStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_ifStatement)
        self._la = 0 # Token type
        try:
            localctx = miniPythonParser.AstIfStatmentContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 81
            self.match(miniPythonParser.IF)
            self.state = 82
            self.expression()
            self.state = 83
            self.match(miniPythonParser.COLON)
            self.state = 84
            self.sequence()
            self.state = 88
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==miniPythonParser.ELSE:
                self.state = 85
                self.match(miniPythonParser.ELSE)
                self.state = 86
                self.match(miniPythonParser.COLON)
                self.state = 87
                self.sequence()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class WhileStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_whileStatement

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstWhileStatmentContext(WhileStatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.WhileStatementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def WHILE(self):
            return self.getToken(miniPythonParser.WHILE, 0)
        def expression(self):
            return self.getTypedRuleContext(miniPythonParser.ExpressionContext,0)

        def COLON(self):
            return self.getToken(miniPythonParser.COLON, 0)
        def sequence(self):
            return self.getTypedRuleContext(miniPythonParser.SequenceContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstWhileStatment" ):
                return visitor.visitAstWhileStatment(self)
            else:
                return visitor.visitChildren(self)



    def whileStatement(self):

        localctx = miniPythonParser.WhileStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_whileStatement)
        try:
            localctx = miniPythonParser.AstWhileStatmentContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 90
            self.match(miniPythonParser.WHILE)
            self.state = 91
            self.expression()
            self.state = 92
            self.match(miniPythonParser.COLON)
            self.state = 93
            self.sequence()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ForStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_forStatement

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstForStatementContext(ForStatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.ForStatementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def FOR(self):
            return self.getToken(miniPythonParser.FOR, 0)
        def expression(self):
            return self.getTypedRuleContext(miniPythonParser.ExpressionContext,0)

        def IN(self):
            return self.getToken(miniPythonParser.IN, 0)
        def expressionList(self):
            return self.getTypedRuleContext(miniPythonParser.ExpressionListContext,0)

        def COLON(self):
            return self.getToken(miniPythonParser.COLON, 0)
        def sequence(self):
            return self.getTypedRuleContext(miniPythonParser.SequenceContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstForStatement" ):
                return visitor.visitAstForStatement(self)
            else:
                return visitor.visitChildren(self)



    def forStatement(self):

        localctx = miniPythonParser.ForStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_forStatement)
        try:
            localctx = miniPythonParser.AstForStatementContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 95
            self.match(miniPythonParser.FOR)
            self.state = 96
            self.expression()
            self.state = 97
            self.match(miniPythonParser.IN)
            self.state = 98
            self.expressionList()
            self.state = 99
            self.match(miniPythonParser.COLON)
            self.state = 100
            self.sequence()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ReturnStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_returnStatement

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstReturnStatementContext(ReturnStatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.ReturnStatementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def RETURN(self):
            return self.getToken(miniPythonParser.RETURN, 0)
        def expression(self):
            return self.getTypedRuleContext(miniPythonParser.ExpressionContext,0)

        def NEWLINE(self):
            return self.getToken(miniPythonParser.NEWLINE, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstReturnStatement" ):
                return visitor.visitAstReturnStatement(self)
            else:
                return visitor.visitChildren(self)



    def returnStatement(self):

        localctx = miniPythonParser.ReturnStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_returnStatement)
        try:
            localctx = miniPythonParser.AstReturnStatementContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 102
            self.match(miniPythonParser.RETURN)
            self.state = 103
            self.expression()
            self.state = 104
            self.match(miniPythonParser.NEWLINE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrintStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_printStatement

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstPrintStatementContext(PrintStatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.PrintStatementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def PRINT(self):
            return self.getToken(miniPythonParser.PRINT, 0)
        def expression(self):
            return self.getTypedRuleContext(miniPythonParser.ExpressionContext,0)

        def NEWLINE(self):
            return self.getToken(miniPythonParser.NEWLINE, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstPrintStatement" ):
                return visitor.visitAstPrintStatement(self)
            else:
                return visitor.visitChildren(self)



    def printStatement(self):

        localctx = miniPythonParser.PrintStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_printStatement)
        try:
            localctx = miniPythonParser.AstPrintStatementContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 106
            self.match(miniPythonParser.PRINT)
            self.state = 107
            self.expression()
            self.state = 108
            self.match(miniPythonParser.NEWLINE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AssignStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_assignStatement

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstAssignStatmentContext(AssignStatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.AssignStatementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def IDENTIFIER(self):
            return self.getToken(miniPythonParser.IDENTIFIER, 0)
        def ASSIGN(self):
            return self.getToken(miniPythonParser.ASSIGN, 0)
        def expression(self):
            return self.getTypedRuleContext(miniPythonParser.ExpressionContext,0)

        def NEWLINE(self):
            return self.getToken(miniPythonParser.NEWLINE, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstAssignStatment" ):
                return visitor.visitAstAssignStatment(self)
            else:
                return visitor.visitChildren(self)



    def assignStatement(self):

        localctx = miniPythonParser.AssignStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_assignStatement)
        try:
            localctx = miniPythonParser.AstAssignStatmentContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 110
            self.match(miniPythonParser.IDENTIFIER)
            self.state = 111
            self.match(miniPythonParser.ASSIGN)
            self.state = 112
            self.expression()
            self.state = 113
            self.match(miniPythonParser.NEWLINE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunctionCallStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_functionCallStatement

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstFunctionCallStatementContext(FunctionCallStatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.FunctionCallStatementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def IDENTIFIER(self):
            return self.getToken(miniPythonParser.IDENTIFIER, 0)
        def BLEFT(self):
            return self.getToken(miniPythonParser.BLEFT, 0)
        def expressionList(self):
            return self.getTypedRuleContext(miniPythonParser.ExpressionListContext,0)

        def BRIGHT(self):
            return self.getToken(miniPythonParser.BRIGHT, 0)
        def NEWLINE(self):
            return self.getToken(miniPythonParser.NEWLINE, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstFunctionCallStatement" ):
                return visitor.visitAstFunctionCallStatement(self)
            else:
                return visitor.visitChildren(self)



    def functionCallStatement(self):

        localctx = miniPythonParser.FunctionCallStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_functionCallStatement)
        try:
            localctx = miniPythonParser.AstFunctionCallStatementContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 115
            self.match(miniPythonParser.IDENTIFIER)
            self.state = 116
            self.match(miniPythonParser.BLEFT)
            self.state = 117
            self.expressionList()
            self.state = 118
            self.match(miniPythonParser.BRIGHT)
            self.state = 119
            self.match(miniPythonParser.NEWLINE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressionStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_expressionStatement

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstExpressionStatementContext(ExpressionStatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.ExpressionStatementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expressionList(self):
            return self.getTypedRuleContext(miniPythonParser.ExpressionListContext,0)

        def NEWLINE(self):
            return self.getToken(miniPythonParser.NEWLINE, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstExpressionStatement" ):
                return visitor.visitAstExpressionStatement(self)
            else:
                return visitor.visitChildren(self)



    def expressionStatement(self):

        localctx = miniPythonParser.ExpressionStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_expressionStatement)
        try:
            localctx = miniPythonParser.AstExpressionStatementContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 121
            self.expressionList()
            self.state = 122
            self.match(miniPythonParser.NEWLINE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SequenceContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_sequence

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstSequenceContext(SequenceContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.SequenceContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def INDENT(self):
            return self.getToken(miniPythonParser.INDENT, 0)
        def moreStatements(self):
            return self.getTypedRuleContext(miniPythonParser.MoreStatementsContext,0)

        def DEDENT(self):
            return self.getToken(miniPythonParser.DEDENT, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstSequence" ):
                return visitor.visitAstSequence(self)
            else:
                return visitor.visitChildren(self)



    def sequence(self):

        localctx = miniPythonParser.SequenceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_sequence)
        try:
            localctx = miniPythonParser.AstSequenceContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 124
            self.match(miniPythonParser.INDENT)
            self.state = 125
            self.moreStatements()
            self.state = 126
            self.match(miniPythonParser.DEDENT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MoreStatementsContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_moreStatements

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstMoreStatementsContext(MoreStatementsContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.MoreStatementsContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(miniPythonParser.StatementContext)
            else:
                return self.getTypedRuleContext(miniPythonParser.StatementContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstMoreStatements" ):
                return visitor.visitAstMoreStatements(self)
            else:
                return visitor.visitChildren(self)



    def moreStatements(self):

        localctx = miniPythonParser.MoreStatementsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_moreStatements)
        self._la = 0 # Token type
        try:
            localctx = miniPythonParser.AstMoreStatementsContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 129 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 128
                self.statement()
                self.state = 131 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << miniPythonParser.DEF) | (1 << miniPythonParser.IF) | (1 << miniPythonParser.WHILE) | (1 << miniPythonParser.FOR) | (1 << miniPythonParser.RETURN) | (1 << miniPythonParser.PRINT) | (1 << miniPythonParser.LEN) | (1 << miniPythonParser.BLEFT) | (1 << miniPythonParser.SQLEFT) | (1 << miniPythonParser.SUB) | (1 << miniPythonParser.NUM) | (1 << miniPythonParser.IDENTIFIER) | (1 << miniPythonParser.STRING) | (1 << miniPythonParser.NEWLINE))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_expression

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstExpressionContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.ExpressionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def additionExpression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(miniPythonParser.AdditionExpressionContext)
            else:
                return self.getTypedRuleContext(miniPythonParser.AdditionExpressionContext,i)

        def LESS(self, i:int=None):
            if i is None:
                return self.getTokens(miniPythonParser.LESS)
            else:
                return self.getToken(miniPythonParser.LESS, i)
        def HIGHER(self, i:int=None):
            if i is None:
                return self.getTokens(miniPythonParser.HIGHER)
            else:
                return self.getToken(miniPythonParser.HIGHER, i)
        def LESSOREQUAL(self, i:int=None):
            if i is None:
                return self.getTokens(miniPythonParser.LESSOREQUAL)
            else:
                return self.getToken(miniPythonParser.LESSOREQUAL, i)
        def HIGHEROREQUAL(self, i:int=None):
            if i is None:
                return self.getTokens(miniPythonParser.HIGHEROREQUAL)
            else:
                return self.getToken(miniPythonParser.HIGHEROREQUAL, i)
        def EQUAL(self, i:int=None):
            if i is None:
                return self.getTokens(miniPythonParser.EQUAL)
            else:
                return self.getToken(miniPythonParser.EQUAL, i)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstExpression" ):
                return visitor.visitAstExpression(self)
            else:
                return visitor.visitChildren(self)



    def expression(self):

        localctx = miniPythonParser.ExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_expression)
        self._la = 0 # Token type
        try:
            localctx = miniPythonParser.AstExpressionContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 133
            self.additionExpression()
            self.state = 138
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << miniPythonParser.EQUAL) | (1 << miniPythonParser.HIGHER) | (1 << miniPythonParser.LESS) | (1 << miniPythonParser.LESSOREQUAL) | (1 << miniPythonParser.HIGHEROREQUAL))) != 0):
                self.state = 134
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << miniPythonParser.EQUAL) | (1 << miniPythonParser.HIGHER) | (1 << miniPythonParser.LESS) | (1 << miniPythonParser.LESSOREQUAL) | (1 << miniPythonParser.HIGHEROREQUAL))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 135
                self.additionExpression()
                self.state = 140
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AdditionExpressionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_additionExpression

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstAdditionExpressionContext(AdditionExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.AdditionExpressionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def multiplicationExpression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(miniPythonParser.MultiplicationExpressionContext)
            else:
                return self.getTypedRuleContext(miniPythonParser.MultiplicationExpressionContext,i)

        def SUM(self, i:int=None):
            if i is None:
                return self.getTokens(miniPythonParser.SUM)
            else:
                return self.getToken(miniPythonParser.SUM, i)
        def SUB(self, i:int=None):
            if i is None:
                return self.getTokens(miniPythonParser.SUB)
            else:
                return self.getToken(miniPythonParser.SUB, i)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstAdditionExpression" ):
                return visitor.visitAstAdditionExpression(self)
            else:
                return visitor.visitChildren(self)



    def additionExpression(self):

        localctx = miniPythonParser.AdditionExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_additionExpression)
        self._la = 0 # Token type
        try:
            localctx = miniPythonParser.AstAdditionExpressionContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 141
            self.multiplicationExpression()
            self.state = 146
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==miniPythonParser.SUM or _la==miniPythonParser.SUB:
                self.state = 142
                _la = self._input.LA(1)
                if not(_la==miniPythonParser.SUM or _la==miniPythonParser.SUB):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 143
                self.multiplicationExpression()
                self.state = 148
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MultiplicationExpressionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_multiplicationExpression

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstMultiplicationExpressionContext(MultiplicationExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.MultiplicationExpressionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def elementExpression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(miniPythonParser.ElementExpressionContext)
            else:
                return self.getTypedRuleContext(miniPythonParser.ElementExpressionContext,i)

        def MUL(self, i:int=None):
            if i is None:
                return self.getTokens(miniPythonParser.MUL)
            else:
                return self.getToken(miniPythonParser.MUL, i)
        def DIV(self, i:int=None):
            if i is None:
                return self.getTokens(miniPythonParser.DIV)
            else:
                return self.getToken(miniPythonParser.DIV, i)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstMultiplicationExpression" ):
                return visitor.visitAstMultiplicationExpression(self)
            else:
                return visitor.visitChildren(self)



    def multiplicationExpression(self):

        localctx = miniPythonParser.MultiplicationExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_multiplicationExpression)
        self._la = 0 # Token type
        try:
            localctx = miniPythonParser.AstMultiplicationExpressionContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 149
            self.elementExpression()
            self.state = 154
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==miniPythonParser.MUL or _la==miniPythonParser.DIV:
                self.state = 150
                _la = self._input.LA(1)
                if not(_la==miniPythonParser.MUL or _la==miniPythonParser.DIV):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 151
                self.elementExpression()
                self.state = 156
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ElementExpressionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_elementExpression

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstElementExpressionContext(ElementExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.ElementExpressionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def primitiveExpression(self):
            return self.getTypedRuleContext(miniPythonParser.PrimitiveExpressionContext,0)

        def SQLEFT(self, i:int=None):
            if i is None:
                return self.getTokens(miniPythonParser.SQLEFT)
            else:
                return self.getToken(miniPythonParser.SQLEFT, i)
        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(miniPythonParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(miniPythonParser.ExpressionContext,i)

        def SQRIGHT(self, i:int=None):
            if i is None:
                return self.getTokens(miniPythonParser.SQRIGHT)
            else:
                return self.getToken(miniPythonParser.SQRIGHT, i)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstElementExpression" ):
                return visitor.visitAstElementExpression(self)
            else:
                return visitor.visitChildren(self)



    def elementExpression(self):

        localctx = miniPythonParser.ElementExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_elementExpression)
        self._la = 0 # Token type
        try:
            localctx = miniPythonParser.AstElementExpressionContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 157
            self.primitiveExpression()
            self.state = 164
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==miniPythonParser.SQLEFT:
                self.state = 158
                self.match(miniPythonParser.SQLEFT)
                self.state = 159
                self.expression()
                self.state = 160
                self.match(miniPythonParser.SQRIGHT)
                self.state = 166
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressionListContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_expressionList

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstExpressionListContext(ExpressionListContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.ExpressionListContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(miniPythonParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(miniPythonParser.ExpressionContext,i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(miniPythonParser.COMMA)
            else:
                return self.getToken(miniPythonParser.COMMA, i)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstExpressionList" ):
                return visitor.visitAstExpressionList(self)
            else:
                return visitor.visitChildren(self)



    def expressionList(self):

        localctx = miniPythonParser.ExpressionListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_expressionList)
        self._la = 0 # Token type
        try:
            localctx = miniPythonParser.AstExpressionListContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 176
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [miniPythonParser.LEN, miniPythonParser.BLEFT, miniPythonParser.SQLEFT, miniPythonParser.SUB, miniPythonParser.NUM, miniPythonParser.IDENTIFIER, miniPythonParser.STRING]:
                self.state = 167
                self.expression()
                self.state = 172
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==miniPythonParser.COMMA:
                    self.state = 168
                    self.match(miniPythonParser.COMMA)
                    self.state = 169
                    self.expression()
                    self.state = 174
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass
            elif token in [miniPythonParser.BRIGHT, miniPythonParser.SQRIGHT, miniPythonParser.COLON, miniPythonParser.NEWLINE]:
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrimitiveExpressionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_primitiveExpression

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstStringPEASTContext(PrimitiveExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.PrimitiveExpressionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def STRING(self):
            return self.getToken(miniPythonParser.STRING, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstStringPEAST" ):
                return visitor.visitAstStringPEAST(self)
            else:
                return visitor.visitChildren(self)


    class AstBlockPEASTContext(PrimitiveExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.PrimitiveExpressionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def BLEFT(self):
            return self.getToken(miniPythonParser.BLEFT, 0)
        def expression(self):
            return self.getTypedRuleContext(miniPythonParser.ExpressionContext,0)

        def BRIGHT(self):
            return self.getToken(miniPythonParser.BRIGHT, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstBlockPEAST" ):
                return visitor.visitAstBlockPEAST(self)
            else:
                return visitor.visitChildren(self)


    class AstLenPEASTContext(PrimitiveExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.PrimitiveExpressionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LEN(self):
            return self.getToken(miniPythonParser.LEN, 0)
        def BLEFT(self):
            return self.getToken(miniPythonParser.BLEFT, 0)
        def expression(self):
            return self.getTypedRuleContext(miniPythonParser.ExpressionContext,0)

        def BRIGHT(self):
            return self.getToken(miniPythonParser.BRIGHT, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstLenPEAST" ):
                return visitor.visitAstLenPEAST(self)
            else:
                return visitor.visitChildren(self)


    class AstDesignatorPEASTContext(PrimitiveExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.PrimitiveExpressionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def designator(self):
            return self.getTypedRuleContext(miniPythonParser.DesignatorContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstDesignatorPEAST" ):
                return visitor.visitAstDesignatorPEAST(self)
            else:
                return visitor.visitChildren(self)


    class AstListPEASTContext(PrimitiveExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.PrimitiveExpressionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SQLEFT(self):
            return self.getToken(miniPythonParser.SQLEFT, 0)
        def expressionList(self):
            return self.getTypedRuleContext(miniPythonParser.ExpressionListContext,0)

        def SQRIGHT(self):
            return self.getToken(miniPythonParser.SQRIGHT, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstListPEAST" ):
                return visitor.visitAstListPEAST(self)
            else:
                return visitor.visitChildren(self)


    class AstNumPEASTContext(PrimitiveExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.PrimitiveExpressionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def NUM(self):
            return self.getToken(miniPythonParser.NUM, 0)
        def SUB(self):
            return self.getToken(miniPythonParser.SUB, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstNumPEAST" ):
                return visitor.visitAstNumPEAST(self)
            else:
                return visitor.visitChildren(self)



    def primitiveExpression(self):

        localctx = miniPythonParser.PrimitiveExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_primitiveExpression)
        self._la = 0 # Token type
        try:
            self.state = 197
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [miniPythonParser.SUB, miniPythonParser.NUM]:
                localctx = miniPythonParser.AstNumPEASTContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 179
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==miniPythonParser.SUB:
                    self.state = 178
                    self.match(miniPythonParser.SUB)


                self.state = 181
                self.match(miniPythonParser.NUM)
                pass
            elif token in [miniPythonParser.STRING]:
                localctx = miniPythonParser.AstStringPEASTContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 182
                self.match(miniPythonParser.STRING)
                pass
            elif token in [miniPythonParser.IDENTIFIER]:
                localctx = miniPythonParser.AstDesignatorPEASTContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 183
                self.designator()
                pass
            elif token in [miniPythonParser.BLEFT]:
                localctx = miniPythonParser.AstBlockPEASTContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 184
                self.match(miniPythonParser.BLEFT)
                self.state = 185
                self.expression()
                self.state = 186
                self.match(miniPythonParser.BRIGHT)
                pass
            elif token in [miniPythonParser.SQLEFT]:
                localctx = miniPythonParser.AstListPEASTContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 188
                self.match(miniPythonParser.SQLEFT)
                self.state = 189
                self.expressionList()
                self.state = 190
                self.match(miniPythonParser.SQRIGHT)
                pass
            elif token in [miniPythonParser.LEN]:
                localctx = miniPythonParser.AstLenPEASTContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 192
                self.match(miniPythonParser.LEN)
                self.state = 193
                self.match(miniPythonParser.BLEFT)
                self.state = 194
                self.expression()
                self.state = 195
                self.match(miniPythonParser.BRIGHT)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DesignatorContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return miniPythonParser.RULE_designator

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class AstDesignatorASTContext(DesignatorContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a miniPythonParser.DesignatorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def IDENTIFIER(self):
            return self.getToken(miniPythonParser.IDENTIFIER, 0)
        def BLEFT(self):
            return self.getToken(miniPythonParser.BLEFT, 0)
        def expressionList(self):
            return self.getTypedRuleContext(miniPythonParser.ExpressionListContext,0)

        def BRIGHT(self):
            return self.getToken(miniPythonParser.BRIGHT, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAstDesignatorAST" ):
                return visitor.visitAstDesignatorAST(self)
            else:
                return visitor.visitChildren(self)



    def designator(self):

        localctx = miniPythonParser.DesignatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_designator)
        self._la = 0 # Token type
        try:
            localctx = miniPythonParser.AstDesignatorASTContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 199
            self.match(miniPythonParser.IDENTIFIER)
            self.state = 204
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==miniPythonParser.BLEFT:
                self.state = 200
                self.match(miniPythonParser.BLEFT)
                self.state = 201
                self.expressionList()
                self.state = 202
                self.match(miniPythonParser.BRIGHT)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





