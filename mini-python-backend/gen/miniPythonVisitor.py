# Generated from C:/Users/geral/Desktop/Proyecto/mini-python/mini-python-backend\miniPython.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .miniPythonParser import miniPythonParser
else:
    from miniPythonParser import miniPythonParser

# This class defines a complete generic visitor for a parse tree produced by miniPythonParser.

class miniPythonVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by miniPythonParser#astProgram.
    def visitAstProgram(self, ctx:miniPythonParser.AstProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astStatementDef.
    def visitAstStatementDef(self, ctx:miniPythonParser.AstStatementDefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#statement.
    def visitStatement(self, ctx:miniPythonParser.StatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astDefStatment.
    def visitAstDefStatment(self, ctx:miniPythonParser.AstDefStatmentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astArgList.
    def visitAstArgList(self, ctx:miniPythonParser.AstArgListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astIfStatment.
    def visitAstIfStatment(self, ctx:miniPythonParser.AstIfStatmentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astWhileStatment.
    def visitAstWhileStatment(self, ctx:miniPythonParser.AstWhileStatmentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astForStatement.
    def visitAstForStatement(self, ctx:miniPythonParser.AstForStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astReturnStatement.
    def visitAstReturnStatement(self, ctx:miniPythonParser.AstReturnStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astPrintStatement.
    def visitAstPrintStatement(self, ctx:miniPythonParser.AstPrintStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astAssignStatment.
    def visitAstAssignStatment(self, ctx:miniPythonParser.AstAssignStatmentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astFunctionCallStatement.
    def visitAstFunctionCallStatement(self, ctx:miniPythonParser.AstFunctionCallStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astExpressionStatement.
    def visitAstExpressionStatement(self, ctx:miniPythonParser.AstExpressionStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astSequence.
    def visitAstSequence(self, ctx:miniPythonParser.AstSequenceContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astMoreStatements.
    def visitAstMoreStatements(self, ctx:miniPythonParser.AstMoreStatementsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astExpression.
    def visitAstExpression(self, ctx:miniPythonParser.AstExpressionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astAdditionExpression.
    def visitAstAdditionExpression(self, ctx:miniPythonParser.AstAdditionExpressionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astMultiplicationExpression.
    def visitAstMultiplicationExpression(self, ctx:miniPythonParser.AstMultiplicationExpressionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astElementExpression.
    def visitAstElementExpression(self, ctx:miniPythonParser.AstElementExpressionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astExpressionList.
    def visitAstExpressionList(self, ctx:miniPythonParser.AstExpressionListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astNumPEAST.
    def visitAstNumPEAST(self, ctx:miniPythonParser.AstNumPEASTContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astStringPEAST.
    def visitAstStringPEAST(self, ctx:miniPythonParser.AstStringPEASTContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astDesignatorPEAST.
    def visitAstDesignatorPEAST(self, ctx:miniPythonParser.AstDesignatorPEASTContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astBlockPEAST.
    def visitAstBlockPEAST(self, ctx:miniPythonParser.AstBlockPEASTContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astListPEAST.
    def visitAstListPEAST(self, ctx:miniPythonParser.AstListPEASTContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astLenPEAST.
    def visitAstLenPEAST(self, ctx:miniPythonParser.AstLenPEASTContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by miniPythonParser#astDesignatorAST.
    def visitAstDesignatorAST(self, ctx:miniPythonParser.AstDesignatorASTContext):
        return self.visitChildren(ctx)



del miniPythonParser