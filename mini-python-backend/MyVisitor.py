from gen.miniPython import miniPython
from gen.miniPythonVisitor import miniPythonVisitor
from SymbolTable import SymbolTable


class MyVisitor(miniPythonVisitor):
    class Instr:
        def __init__(self, i, a):
            self.instr = i
            self.arg = a

    def __init__(self):
        self.table = SymbolTable()
        self.errors = []

        self.codigo = []
        self.instActual = 0
        self.variablesLocalesDefinidas = []

    def generate(self, instr, arg):
        self.codigo.append(MyVisitor.Instr(instr, arg))
        self.instActual += 1

    def printCode(self):
        print("----- CODIGO GENERADO ------\n")
        index = 0
        for x in self.codigo:
            print(str(index) + " " + x.instr, end = '')
            index+=1
            if (x.arg):
                print(" " + x.arg)
            else:
                print("")

    def agregarVariableDefinida(self, nombre, lista):
        if (self.buscarVariableDefinida(nombre,lista) == False):
            lista.append(nombre)

    def buscarVariableDefinida(self, nombre, lista):
        for v in lista:
            if v == nombre:
                return True
        return False


    def visitAstProgram(self, ctx: miniPython.AstProgramContext):
        # print(ctx.__class__.__name__)
        # for i in range(len(ctx.statement())):
        #     self.visit(ctx.statement(i))
        # return None
        super().visitAstProgram(ctx)
        self.printCode()
        return self.codigo

    def visitAstStatementDef(self, ctx: miniPython.AstStatementDefContext):
        if (ctx.IDENTIFIER().getText() == "main"):
            self.generate("DEF", "Main")
        else:
            self.generate("DEF", ctx.IDENTIFIER().getText())
        if (ctx.argList()):
            self.visit(ctx.argList())

        self.visit(ctx.sequence())
        if (ctx.IDENTIFIER().getText() == "main"):
            self.generate("END", None)
        else:
            self.generate("RETURN", None)

        return None

    def visitAstStatementIf(self, ctx: miniPython.AstStatementIfContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.ifStatement())
        return None

    def visitAstStatementReturn(self, ctx: miniPython.AstStatementReturnContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.returnStatement())
        return None

    def visitAstStatementPrint(self, ctx: miniPython.AstStatementPrintContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.printStatement())
        return None

    def visitAstStatementWhile(self, ctx: miniPython.AstStatementWhileContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.whileStatement())
        return None

    def visitAstSststatementFor(self, ctx: miniPython.AstSststatementForContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.forStatement())
        return None

    def visitAstStatementAssign(self, ctx: miniPython.AstStatementAssignContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.assignStatment())
        return None

    def visitAstStatementFuction(self, ctx: miniPython.AstStatementFuctionContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.functionCallStatement())
        return None

    def visitAstStatementExpression(self, ctx: miniPython.AstStatementExpressionContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.expressionStatement())
        return None

    def visitAstDefStatmentArgs(self, ctx: miniPython.AstDefStatmentArgsContext):
        # DEF IDENTIFIER BLEFT argList BRIGHT COLON sequence
        # print(ctx.__class__.__name__)
        self.table.openScope()
        self.table.insertTable(ctx.IDENTIFIER(), self.table.getLevel(), ctx, 1)
        self.visit(ctx.argList())
        self.visit(ctx.sequence())
        self.table.closeScope()
        return None

    def visitAstDefStatment(self, ctx: miniPython.AstDefStatmentContext):
        # DEF IDENTIFIER BLEFT BRIGHT COLON sequence
        # print(ctx.__class__.__name__)
        self.table.openScope()
        self.table.insertTable(ctx.IDENTIFIER(), self.table.getLevel(), ctx, 1)
        self.visit(ctx.sequence())
        self.table.closeScope()
        return None

    def visitAstArgListSingle(self, ctx: miniPython.AstArgListSingleContext):
        # print(ctx.__class__.__name__)
        item = self.table.search(ctx.parentCtx.IDENTIFIER())
        if item.isMethod == 1:
            argList = []
            self.table.insertTable(ctx.IDENTIFIER(), self.table.getLevel(), ctx, 0)
            argList.append(ctx.IDENTIFIER())
            item.setValue(argList)
        return None

    def visitAstArgListMultiple(self, ctx: miniPython.AstArgListMultipleContext):
        # print(ctx.__class__.__name__)
        item = self.table.search(ctx.parentCtx.IDENTIFIER())
        if item.isMethod == 1:
            argList = []
            for i in ctx.IDENTIFIER():
                self.table.insertTable(i, self.table.getLevel(), ctx, 0)
                argList.append(i)
            item.setValue(argList)
        return super().visitAstArgListMultiple(ctx)

    def visitAstIfStatment(self, ctx: miniPython.AstIfStatmentContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.expression())
        self.visit(ctx.sequence(0))
        self.visit(ctx.sequence(1))
        return None

    def visitAstWhileStatment(self, ctx: miniPython.AstWhileStatmentContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.expression())
        self.visit(ctx.sequence())
        return None

    def visitAstForStatement(self, ctx: miniPython.AstForStatementContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.expression())
        self.visit(ctx.expressionList())
        self.visit(ctx.sequence())
        return None

    def visitAstReturnStatement(self, ctx: miniPython.AstReturnStatementContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.expression())
        return None

    def visitAstPrintStatement(self, ctx: miniPython.AstPrintStatementContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.expression())
        return None

    def visitAstAssignStatment(self, ctx: miniPython.AstAssignStatmentContext):
        # print(ctx.__class__.__name__)
        self.table.insertTable(ctx.IDENTIFIER(), self.table.getLevel(), ctx, 1)
        self.visit(ctx.expression())
        return None

    def visitAstFunctionCallStatementArgs(self, ctx: miniPython.AstFunctionCallStatementArgsContext):
        # IDENTIFIER BLEFT expressionList BRIGHT
        item = self.table.search(ctx.IDENTIFIER())
        if item is None:
            self.errors.append("ERROR CONTEXTO - No existe el método " + str(ctx.IDENTIFIER()))
        else:
            self.visit(ctx.expressionList())
        return None

    def visitAstFunctionCallStatement(self, ctx: miniPython.AstFunctionCallStatementContext):
        # IDENTIFIER BLEFT BRIGHT
        item = self.table.search(ctx.IDENTIFIER())
        if item is None:
            self.errors.append("ERROR CONTEXTO - No existe el método " + str(ctx.IDENTIFIER()))
        else:
            self.visit(ctx.expressionList())
        return None

    def visitAstExpressionStatement(self, ctx: miniPython.AstExpressionStatementContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.expressionList())
        return None

    def visitAstSequence(self, ctx: miniPython.AstSequenceContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.moreStatements())
        return None

    def visitAstMoreStatements(self, ctx: miniPython.AstMoreStatementsContext):
        # print(ctx.__class__.__name__)
        for i in range(len(ctx.statement())):
            self.visit(ctx.statement(i))
        return None

    def visitAstExpression(self, ctx: miniPython.AstExpressionContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.additionExpression())
        self.visit(ctx.comparison())
        return None

    def visitAstComparison(self, ctx: miniPython.AstComparisonContext):
        # print(ctx.__class__.__name__)
        for i in range(len(ctx.additionExpression())):
            self.visit(ctx.additionExpression(i))
        return None

    def visitAstAdditionExpression(self, ctx: miniPython.AstAdditionExpressionContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.multiplicationExpression())
        self.visit(ctx.additionFactor())
        return None

    def visitAstAdditionFactor(self, ctx: miniPython.AstAdditionFactorContext):
        # print(ctx.__class__.__name__)
        for i in range(len(ctx.multiplicationExpression())):
            self.visit(ctx.multiplicationExpression(i))
        return None

    def visitAstMultiplicationExpression(self, ctx: miniPython.AstMultiplicationExpressionContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.elementExpression())
        self.visit(ctx.multiplicationFactor())
        return None

    def visitAstMultiplicationFactor(self, ctx: miniPython.AstMultiplicationFactorContext):
        # print(ctx.__class__.__name__)
        for i in range(len(ctx.elementExpression())):
            self.visit(ctx.elementExpression(i))
        return None

    def visitAstElementExpression(self, ctx: miniPython.AstElementExpressionContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.primitiveExpression())
        self.visit(ctx.elementAccess())
        return None

    def visitAstElementAccess(self, ctx: miniPython.AstElementAccessContext):
        # print(ctx.__class__.__name__)
        for i in range(len(ctx.expression())):
            self.visit(ctx.expression(i))
        return None

    def visitAstExpressionList(self, ctx: miniPython.AstExpressionListContext):
        if (ctx.parentCtx.__class__.__name__ == "AstFunctionCallStatementArgsContext"):
            item = self.table.search(ctx.parentCtx.IDENTIFIER())
            argList = []
            argList.append(ctx.expression())
            if (ctx.moreExpressions(0)):
                for i in ctx.moreExpressions(0).COMMA():
                    argList.append(i)
            if (len(item.value) != len(argList)):
                self.errors.append("ERROR CONTEXTO - Se esperaban " + str(len(item.value)) +
                                   " párametros y se ingresaron " + str(len(argList)) +
                                   " en el método " + str(ctx.parentCtx.IDENTIFIER()))
        return None

    def visitAstMoreExpressions(self, ctx: miniPython.AstMoreExpressionsContext):
        # print(ctx.__class__.__name__)
        for i in range(len(ctx.expression())):
            self.visit(ctx.expression(i))
        return None

    def visitAstPrimitiveInt(self, ctx: miniPython.AstPrimitiveIntContext):
        # print(ctx.__class__.__name__)
        return super().visitAstPrimitiveInt(ctx)

    def visitAstPrimitiveFloat(self, ctx: miniPython.AstPrimitiveFloatContext):
        # print(ctx.__class__.__name__)
        return super().visitAstPrimitiveFloat(ctx)

    def visitAstPrimitiveString(self, ctx: miniPython.AstPrimitiveStringContext):
        print(ctx.__class__.__name__)
        return super().visitAstPrimitiveString(ctx)

    def visitAstPrimitiveIdBr(self, ctx: miniPython.AstPrimitiveIdBrContext):
        # IDENTIFIER ( BLEFT expressionList BRIGHT)*
        # print(ctx.__class__.__name__)
        item = self.table.search(ctx.IDENTIFIER())
        if item is None:
            self.errors.append("ERROR CONTEXTO - No existe la variable " + str(ctx.IDENTIFIER()))
        elif item.getLevel() != self.table.getLevel():
            self.errors.append("ERROR CONTEXTO - Variable " + str(ctx.IDENTIFIER()) + " fuera de alcance")
        for i in range(len(ctx.expressionList())):
            self.visit(ctx.expressionList(i))
        return None

    def visitAstPrimitiveIdSq(self, ctx: miniPython.AstPrimitiveIdSqContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.expressionList())
        return None

    def visitAstPrimitiveExp(self, ctx: miniPython.AstPrimitiveExpContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.expression())
        return None

    def visitAstPrimitiveListExp(self, ctx: miniPython.AstPrimitiveListExpContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.listExpression())
        return None

    def visitAstPrimitiveBrExp(self, ctx: miniPython.AstPrimitiveBrExpContext):
        # print(ctx.__class__.__name__)
        self.visit(ctx.expression())
        return None

    def visitAstListExpression(self, ctx: miniPython.AstListExpressionContext):
        self.visit(ctx.expressionList())
        return None
