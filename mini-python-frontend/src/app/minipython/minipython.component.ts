import {
  AfterViewInit,
  Component,
  ElementRef,
  ViewChild,
  OnInit,
} from '@angular/core';
import * as ace from 'ace-builds';
import { saveAs } from 'file-saver';
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpRequest,
} from '@angular/common/http';

@Component({
  selector: 'app-minipython',
  templateUrl: './minipython.component.html',
  styleUrls: ['./minipython.component.css'],
})
export class MinipythonComponent implements AfterViewInit {
  @ViewChild('editor')
  private editor!: ElementRef<HTMLElement>;
  file: any;
  fileContent: string | ArrayBuffer | null | undefined;
  lexerErrors:any = [];
  parserErrors:any = [];
  otherErrors:any = [];
  tokens: any = [];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    ace.config.set('fontSize', '14px');
    const aceEditor = ace.edit(this.editor.nativeElement);
  }

  fileChanged(event: any) {
    this.file = event.target.files[0];
    var fr = new FileReader();
    const aceEditor = ace.edit(this.editor.nativeElement);

    fr.onload = function (e) {
      const resultAsArray = fr.result!.toString().split(/[\r\n]+/);
      resultAsArray.forEach((line) => {
        aceEditor.insert(line + '\n');
      });
    };
    fr.readAsText(this.file);
  }

  onSave() {
    const aceEditor = ace.edit(this.editor.nativeElement);
    var file = new File([aceEditor.getValue()], 'run.txt', {
      type: 'text/plain;charset=utf-8',
    });
    saveAs(file);
  }

  async onRun() {
    try {
      const aceEditor = ace.edit(this.editor.nativeElement);
      var file = new File([aceEditor.getValue()], 'run.txt', {
        type: 'text/plain;charset=utf-8',
      });
      const formData = new FormData();
      formData.append('file',file)

      await this.http
        .post('http://localhost:5000/checkFile', formData)
        .subscribe((data) => {
          this.lexerErrors = data['errors']['lexerErrors']
          this.parserErrors = data['errors']['parserErrors']
          this.otherErrors = data['errors']['otherErrors']
          this.tokens = data['tokens']
        });
    } catch (e) {
      console.log(e);
    }
  }
}
